import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProductDetailComponent } from './components/learn/product-detail/product-detail.component';
import { ProductListComponent } from './components/learn/product-list/product-list.component';
import { AuthGuard } from './guard/auth.guard';
import { LoginComponent } from './components/auth/login/login.component';
import { PageNotFoundComponent } from './components/common/page-not-found/page-not-found.component';
import { ProductAddComponent } from './components/learn/product-add/product-add.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { ListTaskComponent } from './components/task/list-task/list-task.component';
import { CreateTaskComponent } from './components/task/create-task/create-task.component';
import { LogoutComponent } from './components/auth/logout/logout.component';
import { UpdateTaskComponent } from './components/task/update-task/update-task.component';

const routes: Routes = [
  // {
  //   path: 'product-list',
  //   component: ProductListComponent,
  //   canActivate: [AuthGuard],
  // },
  // { path: 'product-detail/:id', component: ProductDetailComponent },
  // { path: 'product-detail', component: ProductDetailComponent },
  // { path: 'admin', component: DashboardComponent, canActivate: [AuthGuard] },
  // { path: 'product-add', component: ProductAddComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'register', component: RegisterComponent },
  {
    path: 'task',
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'list',
        component: ListTaskComponent,
      },
      {
        path: 'create',
        component: CreateTaskComponent,
      },
      {
        path: 'update/:taskId',
        component: UpdateTaskComponent,
      },
    ],
  },
  { path: '', pathMatch: 'full', redirectTo: 'task/list' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
