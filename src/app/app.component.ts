import { AfterViewInit, Component } from '@angular/core';
import { IInventor, IProduct } from './types/interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  ngAfterViewInit(): void {
    this.render(this.show)
  }
  public product: IProduct = {
    name: "Hien",
    quantity: 200,
    price: 200
  }

  public innentors: IInventor[] = [
    {id:1, first: "M", last: "M", year: 1111, passed: 2222},
    {id:2, first: "M", last: "M", year: 1111, passed: 2222},
    {id:3, first: "M", last: "M", year: 1111, passed: 2222}
  ]

  show(innerHTML: string) {
    let item = document.querySelector('#list')

    if(item) {
      item.innerHTML = innerHTML;
    }
  }
  render(callback: (data: string) => any) {
    let innerHTML = this.innentors.map(item=> {
      return `
        <tr>
          <td>${item.id}</td>
          <td>${item.first}</td>
          <td>${item.last}</td>
          <td>${item.year}</td>
          <td>${item.passed}</td>
        </tr>
      `
    }).join('')

    callback(innerHTML)
  }
}
