export interface IProduct {
    name: string;
    quantity: number;
    price: number
}
export interface IInventor {
    id: number;
    first: string;
    last: string;
    year: number;
    passed: number;
}