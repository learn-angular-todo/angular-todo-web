export enum ErrorCode {
  Unknown_Error = 'Unknown_Error',
  Forbidden_Resource = 'Forbidden_Resource',
  Unauthorized = 'Unauthorized',
  Invalid_Input = 'Invalid_Input',
  Email_Or_Password_Not_valid = 'Email_Or_Password_Not_valid',
  Not_Found = 'Not_Found',
  The_Allowed_Number_Of_Calls_Has_Been_Exceeded = 'The_Allowed_Number_Of_Calls_Has_Been_Exceeded',
  Not_Found_User = 'Not_Found_User',
  Old_Password_Incorrect = 'Old_Password_Incorrect',
  Duplicate_Old_Password = 'Duplicate_Old_Password',
  Email_Already_Exist = 'Email_Already_Exist',
}

export enum TaskStatus {
  PENDING = 1,
  SUCCESS = 2,
  DELETE = 3,
}

export const TextTaskStatus = {
  [`${TaskStatus.PENDING}`]: 'PENDING',
  [`${TaskStatus.SUCCESS}`]: 'SUCCESS',
  [`${TaskStatus.DELETE}`]: 'DELETE',
};
