import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthApi } from 'src/app/api/auth';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
})
export class LogoutComponent implements OnInit {
  constructor(private authApi: AuthApi,private router: Router) {}

  ngOnInit(): void {
    this.authApi.logout();
    this.router.navigate(['/']);
  }
}
