import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthApi } from 'src/app/api/auth';
import { ErrorCode } from 'src/app/types/enums';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  @ViewChild('registerForm')
  registerForm!: NgForm;

  constructor(private authApi: AuthApi, private router: Router) {}

  ngOnInit(): void {}

  onSubmit() {
    if (!this.registerForm.valid) {
      return;
    }
    const value = this.registerForm.value as {
      email: string;
      password: string;
      phone: string;
      name: string;
    };

    this.authApi.register(value).subscribe({
      next: (val: any) => {
        this.router.navigate(['/login']);
      },
      error: (err: HttpErrorResponse) => {
        const error = err.error;

        if (error) {
          alert(err.error['errorCode']);
        } else {
          alert(ErrorCode.Unknown_Error);
        }
        this.router.navigate(['/register']);
      },
      complete: () => {},
    });
  }

}
