import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthApi } from 'src/app/api/auth';
import { ErrorCode } from 'src/app/types/enums';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  @ViewChild('loginForm')
  loginForm!: NgForm;

  constructor(private authApi: AuthApi, private router: Router) {}

  ngOnInit(): void {}

  onSubmit() {
    if (!this.loginForm.valid) {
      return;
    }
    const value = this.loginForm.value as { email: string; password: string };

    this.authApi.login(value.email, value.password).subscribe({
      next: (val: any) => {
        const result = val.data;
        const { token, refreshToken } = result;
        this.authApi.setCookie('token', token);
        this.authApi.setCookie('refreshToken', refreshToken);

        this.router.navigate(['/']);
      },
      error: (err: HttpErrorResponse) => {
        const error = err.error;

        if (error) {
          alert(err.error['errorCode']);
        } else {
          alert(ErrorCode.Unknown_Error);
        }
      },
      complete: () => {},
    });
  }
}
