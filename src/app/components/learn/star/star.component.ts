import { Component, OnInit, EventEmitter, Input, Output, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-star',
  templateUrl: './star.component.html',
  styleUrls: ['./star.component.css']
})
export class StarComponent implements OnInit, OnChanges {
  @Input()
  rating: number = 0;

  @Output()
  ratingClicked: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('changes');
    
  }

  ngOnInit(): void {
    console.log('init');
    
    this.rating = 0
  }

  rate() {
    console.log('Rating')

    this.ratingClicked.emit(`Ratting: ${this.rating}`)
  }
 }
