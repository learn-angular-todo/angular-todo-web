import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css'],
})
export class ProductAddComponent implements OnInit {
  productForm: FormGroup = new FormGroup({
    productCode: new FormControl(null, Validators.required),
    productName: new FormControl('test', [
      Validators.required,
      Validators.minLength(6),
    ]),
  });

  constructor() {}

  ngOnInit(): void {}

  onSubmit() {
    if (this.productForm.invalid) {
      console.log('Invalid data');
      return;
    }

    console.log(this.productForm.value);
  }
}
