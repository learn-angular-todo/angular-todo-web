import { Component, OnInit } from '@angular/core';
import { Product } from '../product-list/product-list.class';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css'],
})
export class ProductDetailComponent implements OnInit {
  product: Product = new Product('', '');
  constructor(private route: ActivatedRoute) {
    if (route.snapshot.params['id']) {
      this.product.productCode = route.snapshot.params['id'];
    }
  }

  ngOnInit(): void {}

  changeDetail(form: NgForm) {
    console.log(form.value);

    this.product.productCode = form.value.productCode;
    this.product.productCode = form.value.productCode.toUpperCase();
  }
}
