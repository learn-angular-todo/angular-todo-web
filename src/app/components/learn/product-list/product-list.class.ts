export class Product {
  productCode: string;
  productName: string;
  startRating: number = 0;

  constructor(productCode: string, productName: string) {
    this.productCode = productCode;
    this.productName = productName;
  }
}
