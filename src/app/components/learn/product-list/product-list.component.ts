import { Component, OnInit } from '@angular/core';
import { Product } from './product-list.class';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css'],
})
export class ProductListComponent implements OnInit {
  products: Product[] = [
    new Product('1', 'mot'),
    new Product('2', 'hai'),
    new Product('3', 'ba'),
  ];
  constructor() {}

  ngOnInit(): void {
    console.log(this.products);
  }

  changeStarRating(message: string) {
    console.log(`Message: ${message}`)
  }
}
