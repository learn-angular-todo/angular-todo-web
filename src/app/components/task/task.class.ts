export class Task {
  id!: number;
  userId!: number;
  title!: string;
  description!: string;
  status!: number;
  createdAt!: Date;
  updatedAt!: Date;

  constructor() {}
}
