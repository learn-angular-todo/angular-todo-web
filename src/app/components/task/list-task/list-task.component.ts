import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TaskApi } from 'src/app/api/task';
import { TaskStatus, TextTaskStatus } from 'src/app/types/enums';
import { Task } from '../task.class';
import { PageEvent } from '@angular/material/paginator';
import {
  MatDialog,
  MatDialogConfig,
  MAT_DIALOG_DATA,
  MatDialogRef,
} from '@angular/material/dialog';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-list-task',
  templateUrl: './list-task.component.html',
  styleUrls: ['./list-task.component.css'],
})
export class ListTaskComponent implements OnInit {
  tasks!: Task[];
  TaskStatus = TaskStatus;
  TextTaskStatus = TextTaskStatus;
  page!: {
    pageIndex: number;
    totalPages: number;
    totalItems: number;
    pageSize: 10;
  };

  pageSizeOptions = [5, 10, 25, 100];

  constructor(
    private readonly taskApi: TaskApi,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.taskApi.getListTask().subscribe((result: any) => {
      const { data, ...page } = result;
      this.tasks = data;
      this.page = { ...page, pageSize: 10, pageIndex: page.pageIndex - 1 };
    });
  }

  deleteTask(taskId: number) {
    this.taskApi
      .updateTask(taskId, { status: TaskStatus.DELETE })
      .subscribe((data: any) => {
        // this.tasks = this.tasks.filter((item) => item.id !== taskId);
        this.taskApi
          .getListTask({
            pageIndex: this.page.pageIndex + 1,
            pageSize: this.page.pageSize,
          })
          .subscribe((result: any) => {
            const { data, ...page } = result;
            this.tasks = data;
            this.page = {
              ...page,
              pageSize: 10,
              pageIndex: page.pageIndex - 1,
            };
          });
      });
  }

  handlePage(event: PageEvent) {
    this.taskApi
      .getListTask({ pageIndex: event.pageIndex + 1, pageSize: event.pageSize })
      .subscribe((result: any) => {
        const { data, ...page } = result;
        this.tasks = data;
        this.page = { ...page, pageSize: 10, pageIndex: page.pageIndex - 1 };
      });
  }

  openDialog(taskId: number) {
    let dialogConfig = new MatDialogConfig();
    dialogConfig = {
      data: { taskId },
      height: '50%',
      width: '50%',
    };
    const dialogRef = this.dialog.open(TaskDetailComponent, dialogConfig);

    dialogRef.afterClosed().subscribe((result) => {
      this.taskApi
        .getListTask({
          pageIndex: this.page.pageIndex + 1,
          pageSize: this.page.pageSize,
        })
        .subscribe((result: any) => {
          const { data, ...page } = result;
          this.tasks = data;
          this.page = { ...page, pageSize: 10, pageIndex: page.pageIndex - 1 };
        });
    });
  }
}

@Component({
  selector: 'task-detail',
  templateUrl: '../update-task/update-task.component.html',
})
export class TaskDetailComponent implements OnInit {
  task!: Task;
  TaskStatus = TaskStatus;
  taskId!: number;
  TaskStatusOj = [
    {
      status: TaskStatus.SUCCESS,
      text: TextTaskStatus[TaskStatus.SUCCESS],
    },
    {
      status: TaskStatus.DELETE,
      text: TextTaskStatus[TaskStatus.DELETE],
    },
    {
      status: TaskStatus.PENDING,
      text: TextTaskStatus[TaskStatus.PENDING],
    },
  ];
  taskStatusCurrent!: number;

  constructor(
    @Inject(MAT_DIALOG_DATA) data: any,
    private readonly taskApi: TaskApi,
    private router: Router,
    private dialogRef: MatDialogRef<TaskDetailComponent>
  ) {
    this.taskId = data.taskId;
    this.taskApi.detailTask(this.taskId).subscribe((data: any) => {
      const task = data.data;
      this.task = task;
      this.taskStatusCurrent = task.status;
    });
  }

  ngOnInit() {}

  updateTask(form: NgForm) {
    const params = form.value;
    this.taskApi.updateTask(this.taskId, params).subscribe((data: any) => {
      this.dialogRef.close();
    });
  }

  cancelTask() {
    this.dialogRef.close();
  }
}
