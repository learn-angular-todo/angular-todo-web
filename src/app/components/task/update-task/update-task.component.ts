import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskApi } from 'src/app/api/task';
import { TaskStatus, TextTaskStatus } from 'src/app/types/enums';
import { Task } from '../task.class';

@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.css'],
})
export class UpdateTaskComponent implements OnInit {
  task!: Task;
  taskId!: number;
  TaskStatus = TaskStatus;
  TaskStatusOj = [
    {
      status: TaskStatus.SUCCESS,
      text: TextTaskStatus[TaskStatus.SUCCESS],
    },
    {
      status: TaskStatus.DELETE,
      text: TextTaskStatus[TaskStatus.DELETE],
    },
    {
      status: TaskStatus.PENDING,
      text: TextTaskStatus[TaskStatus.PENDING],
    },
  ];
  taskStatusCurrent!: number;


  constructor(
    private activatedRoute: ActivatedRoute,
    private readonly taskApi: TaskApi,
    private router: Router
  ) {
    if (activatedRoute.snapshot.params['taskId']) {
      this.taskId = activatedRoute.snapshot.params['taskId'];
    }

    this.taskApi.detailTask(this.taskId).subscribe((data: any) => {
      const task = data.data;
      this.task = task;
      this.taskStatusCurrent = task.status;
    });
  }

  ngOnInit(): void {}

  updateTask(form: NgForm) {
    const params = form.value;
    this.taskApi.updateTask(this.taskId, params).subscribe((data: any) => {
      this.router.navigate(['/task/list']);
    });
  }

  cancelTask() {
    this.router.navigate(['/task/list']);
  }
}
