import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TaskApi } from 'src/app/api/task';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.css'],
})
export class CreateTaskComponent implements OnInit {
  taskForm: FormGroup = new FormGroup({
    title: new FormControl(null, [Validators.required]),
    description: new FormControl(null, [Validators.required]),
  });

  constructor(private readonly taskApi: TaskApi, private router: Router) {}

  ngOnInit(): void {}

  onSubmit() {
    if (this.taskForm.invalid) {
      return;
    }

    this.taskApi.createTask(this.taskForm.value).subscribe({
      complete: () => {
        this.router.navigate(['/task/list']);
      },
    });
  }
}
