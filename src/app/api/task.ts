import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { TaskStatus } from '../types/enums';
@Injectable({
  providedIn: 'root',
})
export class TaskApi {
  url = 'https://www.tvhien.link';
  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService
  ) {}

  getListTask(params?: { pageIndex?: number; pageSize?: number }) {
    const token = this.cookieService.get('token');
    const headers = new HttpHeaders({
      Authorization: `Bearer ${token}`,
    });

    return this.httpClient.get(`${this.url}/client/task/get-list-task`, {
      headers,
      params,
    });
  }

  createTask(params: { name: string; description: string }) {
    const token = this.cookieService.get('token');
    const headers = new HttpHeaders({
      Authorization: `Bearer ${token}`,
    });

    return this.httpClient.post(
      `${this.url}/client/task/create`,
      { ...params },
      {
        headers,
      }
    );
  }

  updateTask(
    taskId: number,
    params: { name?: string; description?: string; status?: TaskStatus }
  ) {
    const token = this.cookieService.get('token');
    const headers = new HttpHeaders({
      Authorization: `Bearer ${token}`,
    });

    return this.httpClient.put(
      `${this.url}/client/task/update/${taskId}`,
      { ...params },
      {
        headers,
      }
    );
  }

  detailTask(taskId: number) {
    const token = this.cookieService.get('token');
    const headers = new HttpHeaders({
      Authorization: `Bearer ${token}`,
    });

    return this.httpClient.get(`${this.url}/client/task/get-detail/${taskId}`, {
      headers,
    });
  }
}
