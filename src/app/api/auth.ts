import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ErrorCode } from '../types/enums';
@Injectable({
  providedIn: 'root',
})
export class AuthApi {
  url = 'https://www.tvhien.link';
  constructor(
    private httpClient: HttpClient,
    private cookieService: CookieService
  ) {}

  login(email: string, password: string) {
    this.cookieService.deleteAll();
    return this.httpClient.post(`${this.url}/client/auth/login`, {
      email,
      password,
    });
  }

  logout() {
    this.cookieService.deleteAll();
  }

  register(params: {
    email: string;
    password: string;
    phone: string;
    name: string;
  }) {
    Object.keys(params).forEach((key) => {
      if (!params[key as keyof typeof params]) {
        delete params[key as keyof typeof params];
      }
    });
    return this.httpClient.post(`${this.url}/client/auth/register`, {
      ...params,
    });
  }

  refreshToken() {
    const refreshToken = this.getCookie('refreshToken');
    return this.httpClient.post(`${this.url}/client/auth/refresh-token`, {
      refreshToken,
    });
  }

  changePassword(oldPassword: string, newPassword: string) {
    const token = this.cookieService.get(`token`);

    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');
    headers.set('Authorization', `Bearer ${token}`);

    return this.httpClient.put(
      `${this.url}/client/auth/change-password`,
      {
        oldPassword,
        newPassword,
      },
      { headers }
    );
  }

  checkEmailExists(email: string) {
    return this.httpClient.post(`${this.url}/client/auth/check-email-exists`, {
      email,
    });
  }

  setCookie(keyWord: string, value: any) {
    this.cookieService.set(`${keyWord}`, value);
  }

  getCookie(keyWord: string) {
    this.cookieService.get(`${keyWord}`);
  }
}
