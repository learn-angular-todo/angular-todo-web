import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductListComponent } from './components/learn/product-list/product-list.component';
import { ProductDetailComponent } from './components/learn/product-detail/product-detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StarComponent } from './components/learn/star/star.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PageNotFoundComponent } from './components/common/page-not-found/page-not-found.component';
import { LoginComponent } from './components/auth/login/login.component';
import { ProductAddComponent } from './components/learn/product-add/product-add.component';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './components/auth/register/register.component';
import { ListTaskComponent, TaskDetailComponent } from './components/task/list-task/list-task.component';
import { CreateTaskComponent } from './components/task/create-task/create-task.component';
import { LogoutComponent } from './components/auth/logout/logout.component';
import { UpdateTaskComponent } from './components/task/update-task/update-task.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    ProductDetailComponent,
    StarComponent,
    DashboardComponent,
    PageNotFoundComponent,
    LoginComponent,
    ProductAddComponent,
    RegisterComponent,
    ListTaskComponent,
    CreateTaskComponent,
    LogoutComponent,
    UpdateTaskComponent,
    TaskDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatPaginatorModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
