<!-- ----------------------------------------------------------------------- -->
<!--                               Extensions                                -->
<!-- ----------------------------------------------------------------------- -->

Angular Language Service
Angular Snippets
Angular Files

<!-- ----------------------------------------------------------------------- -->
<!--                               angular cli                               -->
<!-- ----------------------------------------------------------------------- -->
npx ng generate c my-component
npx ng g c my-component

npx ng g s my-service

npx ng g route my-router